FROM rust:1.44 as builder

WORKDIR /home/build

# Copy all the sources
COPY Cargo.toml ./Cargo.toml
COPY Cargo.lock ./Cargo.lock
COPY studio ./studio
COPY src ./src

RUN cargo build --release

FROM ubuntu:latest as runner

WORKDIR /home/app

# Install packages and add non-root user
RUN apt-get update && apt-get -y install curl ca-certificates openssl

# Add ssh keys for cloning.
RUN mkdir $HOME/.ssh
ARG ssh_priv_key
RUN echo "${ssh_priv_key}" >> $HOME/.ssh/id_rsa
ARG ssh_pub_key
RUN echo "${ssh_pub_key}" >> $HOME/.ssh/id_rsa.pub

COPY --from=builder /home/build/target/release/jago /usr/bin/jago
RUN chmod +x /usr/bin/jago

ENV JAGO_SOURCE="gitlab.com/jago-contributors/jago"
ENV JAGO_PURPOSE="cache"

HEALTHCHECK --interval=3s CMD [ -e /tmp/.lock ] || exit 1

CMD ["jago"]
