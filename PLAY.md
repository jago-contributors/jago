<img src="/logo.svg" height="32px" />

# Jago

Jago is an easy way to play around with web development.

To use Jago on your computer, you must install the rust programming language on your computer and use it to build Jago on your own. A goal of Jago is to provide you with the ability to build whatever else it is you need into your workflow.

Lucky for us, the rust community has made it really easy to get started ([instructions](https://www.rust-lang.org/tools/install)). You might have to run `source $HOME/.cargo/env` to configure your current shell.

Once you have rust installed, clone this repository via ssh (see below for why).

```
git clone git@github.com/jago-contributors/jago
```

If you encountered an error, then your computer might not be set up to clone via ssh yet, which is required by `jago`, so let's go ahead and set that up. [Follow GitHub's instructions for your operating system here.](https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

Now, we use a tool from rust's toolchain to build and install Jago.

```
cargo install --path ./jago
```

A common error we might see is that a dependency called `openssl` is not installed. Follow [the instructions for your operating system here.](https://docs.rs/openssl/0.10.29/openssl/#automatic) If you don't see any errors, `jago` was installed correctly!

Run the following command to start Jago.

```
jago
```

To see if it worked, open up `http://localhost:3000`. If you see these same instructions, everything is working!

Now you can view files inside of public repositories on GitHub or GitLab. For example, open [http://localhost:3000/github.com/jago-contributors/jago](http://localhost:3000/github.com/jago-contributors/jago) in your browser.

Now in your terminal, navigate to the directory `~/studio`. This is your studio. Jago automatically clones repositories you have been browsing for you here. They all live in the `source` directory. There is already a new clone of this repository in there. It is suggested you delete this directory and move to the one cloned in `~/studio/source/github.com/jago-contributors/jago`.

What now? Explore! A good place to start is in the [playground directory.](playground/README.md)
