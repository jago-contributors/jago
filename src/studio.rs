use std::convert::Infallible;
use std::io::prelude::*;
use std::path::PathBuf;
use std::str;

use pulldown_cmark::{html, Options, Parser};

use warp::http::{Response, StatusCode};

use bytes::Bytes;

use crate::{Context, Purpose};

fn repos_dir(context: Context) -> PathBuf {
    let p = dirs::home_dir().unwrap().join("studio");

    match context.purpose {
        Purpose::Cache => p.join("cache"),
        Purpose::Create => p.join("source"),
    }
}

pub async fn clear(context: Context, path: String) -> Result<impl warp::Reply, Infallible> {
    match context.purpose {
        Purpose::Create => {
            return Ok(Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body(Bytes::from("clear is only valid in cache mode."))
                .unwrap());
        }
        _ => {}
    };

    let info = match RepositoryInfo::parse(context.clone(), &path) {
        Some(i) => i,
        None => {
            return Ok(Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Bytes::from("not found"))
                .unwrap())
        }
    };

    let repo_dir = repos_dir(context.clone()).join(info.key());

    match std::fs::remove_dir_all(&repo_dir) {
        Err(err) => {
            return Ok(Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body(Bytes::from(err.to_string()))
                .unwrap())
        }
        _ => {}
    };

    Ok(Response::builder()
        .status(StatusCode::OK)
        .body(Bytes::from("Repo cleared"))
        .unwrap())
}

pub async fn handle(context: Context, path: String) -> Result<impl warp::Reply, Infallible> {
    match &path[..] {
        "/favicon.ico" => {
            let src = repos_dir(context.clone())
                .join(&context.source)
                .join("favicon.ico");

            match std::fs::File::open(&src) {
                Ok(file) => {
                    let mut buf_reader = std::io::BufReader::new(file);
                    let mut contents = Vec::new();

                    match buf_reader.read_to_end(&mut contents) {
                        Err(err) => {
                            return Ok(Response::builder()
                                .status(StatusCode::NOT_FOUND)
                                .body(Bytes::from(err.to_string()))
                                .unwrap())
                        }
                        _ => {}
                    };

                    Ok(Response::builder()
                        .status(StatusCode::OK)
                        .body(Bytes::from(contents))
                        .unwrap())
                }
                Err(_) => {
                    return Ok(Response::builder()
                        .status(StatusCode::NOT_FOUND)
                        .body(Bytes::from("not found"))
                        .unwrap());
                }
            }
        }
        path @ _ => {
            let (code, bytes) = source(context, path.into());

            Ok(Response::builder().status(code).body(bytes).unwrap())
        }
    }
}

fn source(context: Context, path: String) -> (StatusCode, bytes::Bytes) {
    let info = match RepositoryInfo::parse(context.clone(), &path) {
        Some(i) => i,
        None => return (StatusCode::OK, "not found".into()),
    };

    let repo_dir = repos_dir(context.clone()).join(info.key());

    match std::fs::create_dir_all(&repo_dir) {
        Err(err) => return (StatusCode::INTERNAL_SERVER_ERROR, err.to_string().into()),
        _ => {}
    };

    match git2::Repository::open(&repo_dir) {
        Ok(_) => {}
        Err(_) => {
            let mut callbacks = git2::RemoteCallbacks::new();

            callbacks.credentials(|_url, username_from_url, _allowed_types| {
                git2::Cred::ssh_key(
                    username_from_url.unwrap(),
                    None,
                    &dirs::home_dir().unwrap().join(".ssh").join("id_rsa"),
                    None,
                )
            });

            let want_host = info.host.to_string();

            callbacks.certificate_check(move |_a, got_host| got_host == &want_host);

            let mut fo = git2::FetchOptions::new();
            fo.remote_callbacks(callbacks);

            let mut builder = git2::build::RepoBuilder::new();
            builder.fetch_options(fo);

            match builder.clone(
                &format!("git@{}:{}/{}.git", info.host, info.owner, info.name),
                &repo_dir,
            ) {
                Ok(r) => r,
                Err(err) => return (StatusCode::INTERNAL_SERVER_ERROR, format!("{}", err).into()),
            };
        }
    };

    let resource_path = std::path::PathBuf::from(match &info.resource {
        Some(r) => r,
        None => "README.md",
    });

    let resource_path = repo_dir.join(&resource_path);

    let meta = match std::fs::metadata(&resource_path) {
        Ok(m) => m,
        Err(err) => match err.kind() {
            std::io::ErrorKind::NotFound => {
                return (StatusCode::NOT_FOUND, "not found".into());
            }
            _ => {
                return (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    "internal server error".into(),
                )
            }
        },
    };

    let ext = if meta.is_dir() {
        return (StatusCode::OK, "directory".into());
    } else {
        resource_path
            .extension()
            .map(|e| e.to_string_lossy())
            .unwrap_or("".into())
    };

    let resource = match std::fs::File::open(&resource_path) {
        Ok(i) => i,
        Err(err) => return (StatusCode::INTERNAL_SERVER_ERROR, err.to_string().into()),
    };

    let mut buf_reader = std::io::BufReader::new(resource);
    let mut contents = String::new();

    match buf_reader.read_to_string(&mut contents) {
        Err(err) => return (StatusCode::INTERNAL_SERVER_ERROR, err.to_string().into()),
        _ => {}
    };

    match &ext[..] {
        "md" => {
            let mut bytes = Vec::new();
            let mut w = std::io::Cursor::new(&mut bytes);

            let mut css_path = resource_path.clone();
            css_path.set_extension("css");

            let css = if std::fs::metadata(&css_path).is_ok() {
                let mut file = match std::fs::File::open(&css_path) {
                    Ok(f) => f,
                    Err(err) => {
                        return (
                            StatusCode::INTERNAL_SERVER_ERROR,
                            format!("Error reading css file for this resource: {}", err).into(),
                        )
                    }
                };

                let mut contents = String::new();

                match file.read_to_string(&mut contents) {
                    Ok(_) => {}
                    Err(err) => {
                        return (
                            StatusCode::INTERNAL_SERVER_ERROR,
                            format!("Error reading css file for this resource: {}", err).into(),
                        )
                    }
                };

                contents
            } else {
                "".to_string()
            };

            let mut js_path = resource_path.clone();
            js_path.set_extension("js");

            let js = if std::fs::metadata(&js_path).is_ok() {
                let mut file = match std::fs::File::open(&js_path) {
                    Ok(f) => f,
                    Err(err) => {
                        return (
                            StatusCode::INTERNAL_SERVER_ERROR,
                            format!("Error reading js file for this resource: {}", err).into(),
                        )
                    }
                };

                let mut contents = String::new();

                match file.read_to_string(&mut contents) {
                    Ok(_) => {}
                    Err(err) => {
                        return (
                            StatusCode::INTERNAL_SERVER_ERROR,
                            format!("Error reading js file for this resource: {}", err).into(),
                        )
                    }
                };

                contents
            } else {
                "".to_string()
            };

            write!(
                w,
                "<!DOCTYPE html>
                <html>
                <head>
                    <meta charset=\"utf8\">
                    <title>Jago Studio</title>
                    <style>
                        .container {{
                            max-width: 800px;
                            margin: 0 auto;
                        }}

                        {css}
                    </style>
                    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/4.0.0/github-markdown.min.css\" integrity=\"sha256-gzohnzxILb7OZZch6c8mySnK1r0yFviwmBR+1E5O0RM=\" crossorigin=\"anonymous\" />
                    <body>
                    <div class=\"container markdown-body\">
                    ",
                css = css,
            )
            .unwrap();

            // TODO: check for <x>.before.js

            let parser = Parser::new_ext(&contents, Options::all());
            html::write_html(&mut w, parser).unwrap();

            // TODO: check for <x>.after.js, or <x>.js

            write!(w, "<script>{js}</script></div></body></html>", js = js).unwrap();

            (StatusCode::OK, bytes::Bytes::from(bytes))
        }
        _ => (StatusCode::OK, bytes::Bytes::from(contents)),
    }
}

enum Host {
    GitLab,
    GitHub,
}

impl std::fmt::Display for Host {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::GitLab => write!(f, "gitlab.com"),
            Self::GitHub => write!(f, "github.com"),
        }
    }
}

struct RepositoryInfo {
    host: Host,
    owner: String,
    name: String,
    resource: Option<String>,
}

impl RepositoryInfo {
    fn key(&self) -> String {
        format!("{}/{}/{}", self.host, self.owner, self.name)
    }

    fn remote(&self) -> String {
        format!("https://{}/{}/{}.git", self.host, self.owner, self.name)
    }

    fn parse(context: Context, path: &str) -> Option<RepositoryInfo> {
        let path = &path[1..];

        let path = if !path.starts_with("github.com") && !path.starts_with("gitlab.com") {
            format!("{}/{}", context.source, path)
        } else {
            path.to_string()
        };

        let parts = path.split("/").collect::<Vec<_>>();

        if parts.len() < 3 {
            return None;
        }

        let host = match parts[0] {
            "github.com" => Host::GitHub,
            "gitlab.com" => Host::GitLab,
            _ => return Self::parse(context.clone(), &format!("{}/{}", context.source, path)),
        };

        let owner = parts[1];
        let name = parts[2];

        let resource = match &parts[3..].join("/")[..] {
            "" => None,
            val @ _ => Some(val.to_string()),
        };

        Some(RepositoryInfo {
            host,
            owner: owner.into(),
            name: name.into(),
            resource,
        })
    }
}

#[derive(Debug)]
enum Error {
    InvalidRepository(String),
    UnsupportedHost(String),
    Io(std::io::Error),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::InvalidRepository(msg) => {
                write!(f, "# Error determining repository information\n\n{}", msg)
            }
            Self::UnsupportedHost(msg) => write!(f, "# Repository host not supported\n\n{}", msg),
            Self::Io(err) => write!(f, "# Error reading or writing files\n\n{}", err),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(err) => Some(err),
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{Config, Context};
    use std::sync::Arc;

    fn test_context() -> Context {
        Arc::new(Config {
            source: "github.com/owner/name".into(),
            purpose: Purpose::Cache,
        })
    }

    #[test]
    fn parse_repo_info() {
        let tests = vec![
            (
                "/",
                RepositoryInfo {
                    host: Host::GitHub,
                    owner: "owner".into(),
                    name: "name".into(),
                    resource: None,
                },
            ),
            (
                "/some/resource",
                RepositoryInfo {
                    host: Host::GitHub,
                    owner: "owner".into(),
                    name: "name".into(),
                    resource: Some("some/resource".into()),
                },
            ),
            (
                "/github.com/github/github",
                RepositoryInfo {
                    host: Host::GitHub,
                    owner: "github".into(),
                    name: "github".into(),
                    resource: None,
                },
            ),
            (
                "/gitlab.com/some/gitlab",
                RepositoryInfo {
                    host: Host::GitLab,
                    owner: "some".into(),
                    name: "gitlab".into(),
                    resource: None,
                },
            ),
        ];

        let context = test_context();

        for (path, want) in tests {
            let got = RepositoryInfo::parse(context.clone(), path).unwrap();

            assert_eq!(got.host.to_string(), want.host.to_string());
            assert_eq!(got.owner, want.owner);
            assert_eq!(got.name, want.name);
            assert_eq!(got.resource, want.resource);
        }
    }
}
