mod repository;
mod studio;

use warp::Filter;

#[tokio::main]
async fn main() {
    let context = std::sync::Arc::new(Config {
        source: std::env::var("JAGO_SOURCE").unwrap_or("github.com/jago-contributors/jago".into()),
        purpose: std::env::var("JAGO_PURPOSE")
            .map(|purpose| match &purpose[..] {
                "cache" => Purpose::Cache,
                _ => Purpose::Create,
            })
            .unwrap_or(Purpose::Create),
    });

    let routes = warp::path::full()
        .and(warp::get())
        .and(with_context(context.clone()))
        .map(|path: warp::path::FullPath, context: Context| (context, path.as_str().into()))
        .untuple_one()
        .and_then(studio::handle);

    let clear = warp::path("clear")
        .and(warp::path::full())
        .and(warp::get())
        .and(with_context(context.clone()))
        .map(|path: warp::path::FullPath, context: Context| {
            (context, path.as_str()["/clear/".len()..].into())
        })
        .untuple_one()
        .and_then(studio::clear);

    let routes = clear.or(routes);

    let routes = routes.with(warp::log("jago"));

    println!("Listening to http://localhost:3000");

    warp::serve(routes).run(([0, 0, 0, 0], 3000)).await;
}

enum Purpose {
    Cache,
    Create,
}

pub struct Config {
    /// The repository that this code lives in.
    source: String,
    purpose: Purpose,
}

impl Config {
    fn new(purpose: Purpose) -> Self {
        Self {
            source: std::env::var("JAGO_SOURCE").unwrap(),
            purpose,
        }
    }
}

pub type Context = std::sync::Arc<Config>;

fn with_context(
    context: Context,
) -> impl Filter<Extract = (Context,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || context.clone())
}
