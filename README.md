<img src="/logo.svg" height="42px" />

# Jago

## What

Jago Studio is a place to learn the perspective of others.

To you, the reader, Jago Studio is just a web page that you arrived at by clicking a link from elsewhere. To you, the thinker, Jago Studio is one of the many ways you can share your thoughts with others.

## How

Jago Studio will always be free to everyone. However, Jago Studio recognizes the current state of society and its need for a source of money for it to operate. Because of this, we do accept donations from those who wish to donate to our efforts.

There is no hope of making money off of the thoughts of others. We will not provide a number we hope to reach. We hope an amount of money that is more than we need because we hope to share. The money will be divided as follows:

- A variable amount that is the cost of the resources used to provide Jago Studio for free.
- A small amount will be distributed to contributors of Jago Studio based on the donations made on their behalf.
- The rest will be donated to non-profit organizations that will be determined collectively by the contributors.

If you would like to donate, talk with a contributor.

## Contribute

Contribution to Jago Studio is open to everyone. There are two ways to contribute to Jago Studio.

The first is to use Jago Studio to publish your own content. If someone who has viewed your content then goes on to donate, you will be considered a contributor for that month.

The second is to help maintain the collection of software that is Jago. Each change set will be squashed into one commit before being merged into the repository. This commit will have an explanation of the changes which will be counted as a contribution.
