VERSION="9dcc0c4"

docker build \
   --build-arg ssh_priv_key="$(cat ~/.ssh/jago_rsa)" \
   --build-arg ssh_pub_key="$(cat ~/.ssh/jago_rsa.pub)" \
   --tag jago:$VERSION .

docker tag jago:$VERSION gcr.io/jago-277604/jago:$VERSION
docker push gcr.io/jago-277604/jago:$VERSION
