# Playground

This directory has some basic stuff. We've got this markdown file, and two other files by the same name but with `css` and `js` extensions instead. Whenever a markdown file is served, css and js files by the same name come with it. In addition to this, we have a directory containing an html file in it. It doesn't really matter what type of file it is, if its in your repository `jago` will serve it.

This means all you have to do is create, `jago` will help you with the rest.

## Puzzles

To work on the puzzle(s), just open the file for it in your browser.

- [slow-reveal/index.html](slow-reveal/index.html)
