# Set up Gitolite

GitHub and GitLab are good for free backend, but I don't want to deal with integrating with them for private repository support. Let's use gitolite for this [https://gitolite.com/gitolite/index.html](https://gitolite.com/gitolite/index.html).

# Better Git Integration

Git support is pretty hacky at the moment.
