use std::io::prelude::*;
use std::path::PathBuf;
use std::str;
use std::sync::Arc;

use hyper::{http::StatusCode, Body, Request, Response};
use pulldown_cmark::{html, Event, Options, Parser, Tag};

fn repos_dir() -> PathBuf {
    dirs::home_dir().unwrap().join("studio").join("source")
}

pub fn handle(req: Request<Body>) -> Response<Body> {
    let context = Arc::new(Config::new());

    match req.uri().path() {
        "/favicon.ico" => {
            let src = repos_dir().join(&context.source).join("favicon.ico");

            match std::fs::File::open(&src) {
                Ok(file) => {
                    let mut buf_reader = std::io::BufReader::new(file);
                    let mut contents = Vec::new();

                    match buf_reader.read_to_end(&mut contents) {
                        Err(err) => {
                            return Response::builder()
                                .status(StatusCode::NOT_FOUND)
                                .body(Body::from(err.to_string()))
                                .unwrap()
                        }
                        _ => {}
                    };

                    Response::builder()
                        .status(StatusCode::OK)
                        .body(Body::from(contents))
                        .unwrap()
                }
                Err(_) => {
                    return Response::builder()
                        .status(StatusCode::NOT_FOUND)
                        .body(Body::from("not found"))
                        .unwrap();
                }
            }
        }
        path @ _ => source(context, path.to_string()),
    }
}

fn source(context: Context, path: String) -> Response<Body> {
    let info = match RepositoryInfo::parse(context, &path) {
        Some(i) => i,
        None => {
            return Response::builder()
                .status(hyper::http::StatusCode::NOT_FOUND)
                .body(Body::from("not found"))
                .unwrap();
        }
    };

    let repo_dir = repos_dir().join(info.key());

    match std::fs::create_dir_all(&repo_dir) {
        Err(err) => return Response::new(Body::from(err.to_string())),
        _ => {}
    };

    match git2::Repository::open(&repo_dir) {
        Err(_) => {
            let mut callbacks = git2::RemoteCallbacks::new();

            callbacks.credentials(|_url, username_from_url, _allowed_types| {
                git2::Cred::ssh_key(
                    username_from_url.unwrap(),
                    None,
                    &dirs::home_dir().unwrap().join(".ssh").join("id_rsa"),
                    None,
                )
            });

            let want_host = info.host.to_string();

            callbacks.certificate_check(move |_a, got_host| got_host == &want_host);

            let mut fo = git2::FetchOptions::new();
            fo.remote_callbacks(callbacks);

            let mut builder = git2::build::RepoBuilder::new();
            builder.fetch_options(fo);

            match builder.clone(
                &format!("git@{}:{}/{}.git", info.host, info.owner, info.name),
                &repo_dir,
            ) {
                Ok(r) => r,
                Err(err) => return Response::new(Body::from(err.to_string())),
            };
        }
        _ => {}
    };

    let resource_path = std::path::PathBuf::from(match &info.resource {
        Some(r) => r,
        None => "README.md",
    });

    let resource_path = repo_dir.join(&resource_path);

    let meta = match std::fs::metadata(&resource_path) {
        Ok(m) => m,
        Err(err) => match err.kind() {
            std::io::ErrorKind::NotFound => {
                return Response::builder()
                    .status(hyper::http::StatusCode::NOT_FOUND)
                    .body(Body::from("not found"))
                    .unwrap();
            }
            _ => {
                return Response::builder()
                    .status(hyper::http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::from("internal server error"))
                    .unwrap()
            }
        },
    };

    let ext = if meta.is_dir() {
        return Response::new(Body::from("directory"));
    } else {
        resource_path
            .extension()
            .map(|e| e.to_string_lossy())
            .unwrap_or("".into())
    };

    let resource = match std::fs::File::open(&resource_path) {
        Ok(i) => i,
        Err(err) => return Response::new(Body::from(err.to_string())),
    };

    let mut buf_reader = std::io::BufReader::new(resource);
    let mut contents = String::new();

    match buf_reader.read_to_string(&mut contents) {
        Err(err) => return Response::new(Body::from(err.to_string())),
        _ => {}
    };

    match &ext[..] {
        "md" => {
            let mut bytes = Vec::new();
            let mut w = std::io::Cursor::new(&mut bytes);

            let mut css_path = resource_path.clone();
            css_path.set_extension("css");

            let css = if std::fs::metadata(&css_path).is_ok() {
                let mut file = match std::fs::File::open(&css_path) {
                    Ok(f) => f,
                    Err(err) => {
                        return Response::builder()
                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                            .body(Body::from(format!(
                                "Error reading css file for this resource: {}",
                                err
                            )))
                            .unwrap()
                    }
                };

                let mut contents = String::new();

                match file.read_to_string(&mut contents) {
                    Ok(_) => {}
                    Err(err) => {
                        return Response::builder()
                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                            .body(Body::from(format!(
                                "Error reading css file for this resource: {}",
                                err
                            )))
                            .unwrap()
                    }
                };

                contents
            } else {
                "".to_string()
            };

            let mut js_path = resource_path.clone();
            js_path.set_extension("js");

            let js = if std::fs::metadata(&js_path).is_ok() {
                let mut file = match std::fs::File::open(&js_path) {
                    Ok(f) => f,
                    Err(err) => {
                        return Response::builder()
                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                            .body(Body::from(format!(
                                "Error reading js file for this resource: {}",
                                err
                            )))
                            .unwrap()
                    }
                };

                let mut contents = String::new();

                match file.read_to_string(&mut contents) {
                    Ok(_) => {}
                    Err(err) => {
                        return Response::builder()
                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                            .body(Body::from(format!(
                                "Error reading js file for this resource: {}",
                                err
                            )))
                            .unwrap()
                    }
                };

                contents
            } else {
                "".to_string()
            };

            write!(
                w,
                "<!DOCTYPE html>
                <html>
                <head>
                    <meta charset=\"utf8\">
                    <title>Jago Studio</title>
                    <style>
                        .container {{
                            max-width: 800px;
                            margin: 0 auto;
                        }}

                        {css}
                    </style>
                    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/4.0.0/github-markdown.min.css\" integrity=\"sha256-gzohnzxILb7OZZch6c8mySnK1r0yFviwmBR+1E5O0RM=\" crossorigin=\"anonymous\" />
                    <body>
                    <div class=\"container markdown-body\">
                    ",
                css = css,
            )
            .unwrap();

            // TODO: check for <x>.before.js

            let repo_key = info.key();

            let parser = Parser::new_ext(&contents, Options::all()).map(|event| match &event {
                Event::Start(tag) => match &tag {
                    Tag::Link(link_type, dest, title) => {
                        let dest = match link_type {
                            pulldown_cmark::LinkType::Inline => match url::Url::parse(dest) {
                                Ok(_) => dest.clone().into_string(),
                                _ => format!("/{}/{}", repo_key, dest),
                            },
                            _ => dest.clone().into_string(),
                        };

                        Event::Start(Tag::Link(*link_type, dest.into(), title.clone()))
                    }
                    _ => event,
                },
                Event::Html(node) => {
                    // TODO: parse and fix urls from <img> tags, etc
                    Event::Html(node.clone())
                }
                _ => event,
            });
            html::write_html(&mut w, parser).unwrap();

            // TODO: check for <x>.after.js, or <x>.js

            write!(w, "<script>{js}</script></div></body></html>", js = js).unwrap();

            Response::builder()
                .status(StatusCode::OK)
                .body(Body::from(bytes))
                .unwrap()
        }
        _ => Response::new(Body::from(contents)),
    }
}

struct Config {
    /// The repository that this code lives in.
    source: String,
}

impl Config {
    fn new() -> Self {
        Self {
            source: std::env::var("JAGO_SOURCE").unwrap(),
        }
    }
}

type Context = Arc<Config>;

enum Host {
    GitLab,
    GitHub,
}

impl std::fmt::Display for Host {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::GitLab => write!(f, "gitlab.com"),
            Self::GitHub => write!(f, "github.com"),
        }
    }
}

struct RepositoryInfo {
    host: Host,
    owner: String,
    name: String,
    resource: Option<String>,
}

impl RepositoryInfo {
    fn key(&self) -> String {
        format!("{}/{}/{}", self.host, self.owner, self.name)
    }

    fn remote(&self) -> String {
        format!("https://{}/{}/{}.git", self.host, self.owner, self.name)
    }

    fn parse(context: Context, path: &str) -> Option<RepositoryInfo> {
        let path = &path[1..];

        let path = if !path.starts_with("github.com") && !path.starts_with("gitlab.com") {
            format!("{}/{}", context.source, path)
        } else {
            path.to_string()
        };

        let parts = path.split("/").collect::<Vec<_>>();

        if parts.len() < 3 {
            return None;
        }

        let host = match parts[0] {
            "github.com" => Host::GitHub,
            "gitlab.com" => Host::GitLab,
            _ => return Self::parse(context.clone(), &format!("{}/{}", context.source, path)),
        };

        let owner = parts[1];
        let name = parts[2];

        let resource = match &parts[3..].join("/")[..] {
            "" => None,
            val @ _ => Some(val.to_string()),
        };

        Some(RepositoryInfo {
            host,
            owner: owner.into(),
            name: name.into(),
            resource,
        })
    }
}

#[derive(Debug)]
enum Error {
    InvalidRepository(String),
    UnsupportedHost(String),
    Io(std::io::Error),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::InvalidRepository(msg) => {
                write!(f, "# Error determining repository information\n\n{}", msg)
            }
            Self::UnsupportedHost(msg) => write!(f, "# Repository host not supported\n\n{}", msg),
            Self::Io(err) => write!(f, "# Error reading or writing files\n\n{}", err),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Io(err) => Some(err),
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_context() -> Context {
        Arc::new(Config {
            source: "github.com/owner/name".into(),
        })
    }

    #[test]
    fn parse_repo_info() {
        let tests = vec![
            (
                "/",
                RepositoryInfo {
                    host: Host::GitHub,
                    owner: "owner".into(),
                    name: "name".into(),
                    resource: None,
                },
            ),
            (
                "/some/resource",
                RepositoryInfo {
                    host: Host::GitHub,
                    owner: "owner".into(),
                    name: "name".into(),
                    resource: Some("some/resource".into()),
                },
            ),
            (
                "/github.com/github/github",
                RepositoryInfo {
                    host: Host::GitHub,
                    owner: "github".into(),
                    name: "github".into(),
                    resource: None,
                },
            ),
            (
                "/gitlab.com/some/gitlab",
                RepositoryInfo {
                    host: Host::GitLab,
                    owner: "some".into(),
                    name: "gitlab".into(),
                    resource: None,
                },
            ),
        ];

        let context = test_context();

        for (path, want) in tests {
            let got = RepositoryInfo::parse(context.clone(), path).unwrap();

            assert_eq!(got.host.to_string(), want.host.to_string());
            assert_eq!(got.owner, want.owner);
            assert_eq!(got.name, want.name);
            assert_eq!(got.resource, want.resource);
        }
    }
}
